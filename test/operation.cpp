#include <sstream>
#include <string>

#include "common_test.hpp"

#include <test/myproject/user_name.hpp>
#include <test/myproject/multi_pk.hpp>
#include <test/myproject/document.hpp>
#include <test/myproject/parse_task.hpp>
#include <test/myproject/meta/holder.hpp>

#include <nov/database/database.hpp>
#include <nov/database/backend.hpp>
#include <nov/database/configuration.hpp>
#include <nov/database/stream.hpp>
#include <nov/database/logger.hpp>

#include <nov/xproject/exception.hpp>
#include <nov/xproject/logger.hpp>

#include <nov/log/cout_logger.hpp>
#include <nov/log/default_logger.hpp>

using namespace Test::Project;
using namespace Nov::Database;
typedef Nov::XProject::Exception XExc;

class LoggerScope {
public:
	LoggerScope(): xLogger_(backend_, "Test_XProject"), dbLogger_(backend_, "Test_DB") {
		dbLogger_.setThreshold(Nov::Log::LogLevel::ALL);
		xLogger_.setThreshold(Nov::Log::LogLevel::ALL);
		Nov::XProject::setLogger(&xLogger_);
		Nov::Database::setLogger(&dbLogger_);
	}

	~LoggerScope() {
		Nov::XProject::setLogger(nullptr);
		Nov::Database::setLogger(nullptr);
	}

private:
	Nov::Log::CoutLogger backend_;
	Nov::Log::DefaultLogger xLogger_;
	Nov::Log::DefaultLogger dbLogger_;
};

template<typename T>
class Tester {
public:
	typedef typename T::Operation Operation;
	Tester():
		be("sqlite"),
		conf(be.createConfiguration("test")),
		connection_(conf->newConnection()),
		metas_(createMetaHolder()),
		op(connection_)
	{
		metas_.init();
		metas_.initKeys();

		metas_.apply([this] (Nov::Database::Meta& meta){
			Stream q = connection_->createStream();
			q << "DROP TABLE IF EXISTS " << meta.getTable();
			q.execute();
		});

		metas_.apply([this] (Nov::Database::Meta& meta){
			connection_->createTable(meta);
		});
	}

	Operation& operation() {
		return op;
	}

	std::shared_ptr<Connection> connection() {
		return connection_;
	}

private:
	LoggerScope logger;
	Backend be;
	const std::shared_ptr<Configuration> conf;
	const std::shared_ptr<Connection> connection_;

	Nov::Database::MetaHolder metas_;
	Operation op;
};

BOOST_AUTO_TEST_CASE( testInsert ) {
	Tester<UserName> t;
	UserName o1;
	UserName o2;

	o1.name = "one";
	o2.name = "two";

	BOOST_TEST_PASSPOINT();
	auto all = o1.bindAll();
	for (auto& i: all) {
		std::cout << "n = " << i.getField().name() << std::endl;
	}
	std::cout << "Assign name = " << o1.assignName().getField().name() << std::endl;
	std::cout << "Assign 2 = " << o1.assignUserNameId().getField().name() << std::endl;

	std::cout << "T = " << (o1.meta().getTable().name()) << std::endl;

	t.operation().insert(o1);
	BOOST_TEST_PASSPOINT();
	t.operation().insert(o2);
	BOOST_TEST_PASSPOINT();

	BOOST_REQUIRE(!o1.userNameId.isNull());
	BOOST_REQUIRE(!o2.userNameId.isNull());

	BOOST_CHECK(o1.userNameId < o2.userNameId);
}

BOOST_AUTO_TEST_CASE( testObjectLoad ) {
	Tester<UserName> t;

	UserName o;
	o.name = "Dmitri Novikov";
	BOOST_TEST_PASSPOINT();
	t.operation().insert(o);
	BOOST_TEST_PASSPOINT();

	const UserName l = t.operation().load(o.userNameId);
	BOOST_CHECK_EQUAL(l.name, "Dmitri Novikov");
}

BOOST_AUTO_TEST_CASE( testRemove ) {
	Tester<UserName> t;
	UserName o;
	t.operation().insert(o);

	BOOST_CHECK_NO_THROW(t.operation().remove(o.userNameId));
	// should throw dye to object already removed
	BOOST_CHECK_THROW(t.operation().remove(o.userNameId), XExc);
}

BOOST_AUTO_TEST_CASE( testRemoveNull ) {
	Tester<UserName> t;
	BOOST_CHECK_THROW(t.operation().remove(Id<UserName>()), XExc);
}

BOOST_AUTO_TEST_CASE( testLoadNull ) {
	Tester<UserName> t;
	BOOST_CHECK_THROW(t.operation().load(Id<UserName>()), XExc);
}

BOOST_AUTO_TEST_CASE( testLoadNotExisting ) {
	Tester<UserName> t;
	UserName o;
	t.operation().insert(o);
	t.operation().remove(o.userNameId);

	BOOST_CHECK_THROW(t.operation().load(o.userNameId), XExc);
}

BOOST_AUTO_TEST_CASE( testLoadToNull ) {
	Tester<UserName> t;
	UserName o;
	BOOST_CHECK(!t.operation().loadTo(o));
}

BOOST_AUTO_TEST_CASE( testLoadToNotExisting ) {
	Tester<UserName> t;
	UserName o;
	t.operation().insert(o);
	t.operation().remove(o.userNameId);

	BOOST_CHECK(!t.operation().loadTo(o));
}

BOOST_AUTO_TEST_CASE( testLoadToExisting ) {
	Tester<UserName> t;

	UserName o;
	o.name = "Dmitri Novikov";
	t.operation().insert(o);

	UserName l;
	l.userNameId = o.userNameId;
	BOOST_CHECK(t.operation().loadTo(l));
	BOOST_CHECK_EQUAL(l.name, "Dmitri Novikov");
}

BOOST_AUTO_TEST_CASE( testLoadByUnique ) {
	Tester<UserName> t;

	UserName ivan;
	ivan.name = "Ivan";
	UserName dmitri;
	dmitri.name = "Dmitri";

	t.operation().insert(ivan);
	t.operation().insert(dmitri);

	BOOST_CHECK_EQUAL(t.operation().loadByName("Ivan").userNameId, ivan.userNameId);
	BOOST_CHECK_EQUAL(t.operation().loadByName("Dmitri").userNameId, dmitri.userNameId);

	BOOST_CHECK_THROW(t.operation().loadByName("NoSuchName"), XExc);
}

BOOST_AUTO_TEST_CASE( testLoadToByUnique ) {
	Tester<UserName> t;

	UserName ivan;
	ivan.name = "Ivan";
	UserName dmitri;
	dmitri.name = "Dmitri";

	t.operation().insert(ivan);
	t.operation().insert(dmitri);

	UserName tmp;

	tmp.name = "Ivan";
	BOOST_CHECK(t.operation().loadByNameTo(tmp));
	BOOST_CHECK_EQUAL(tmp.userNameId, ivan.userNameId);

	tmp.name = "Dmitri";
	BOOST_CHECK(t.operation().loadByNameTo(tmp));
	BOOST_CHECK_EQUAL(tmp.userNameId, dmitri.userNameId);

	tmp.name = "NoSuchName";
	BOOST_CHECK(!t.operation().loadByNameTo(tmp));
}

BOOST_AUTO_TEST_CASE( testUpdateNotExisting ) {
	Tester<UserName> t;
	UserName dmitri;
	dmitri.name = "Dmitri";
	auto copy = dmitri;
	copy.name = "1";
	BOOST_CHECK_THROW(t.operation().update(dmitri, copy), XExc);
}

BOOST_AUTO_TEST_CASE( testUpdateSkip ) {
	Tester<UserName> t;
	UserName dmitri;
	dmitri.name = "Dmitri";
	t.operation().insert(dmitri);
	auto copy = dmitri;
	BOOST_CHECK(!t.operation().update(dmitri, copy));
}

BOOST_AUTO_TEST_CASE( testUpdate ) {
	Tester<UserName> t;
	UserName n;
	n.name = "First";
	t.operation().insert(n);
	auto copy = n;
	copy.name = "Second";

	BOOST_CHECK_EQUAL(t.operation().load(n.userNameId).name, "First");
	BOOST_CHECK(t.operation().update(n, copy));
	BOOST_CHECK_EQUAL(t.operation().load(n.userNameId).name, "Second");
}

BOOST_AUTO_TEST_CASE( multiPk ) {
	Tester<MultiPk> t;
	auto c = t.connection();
	UserNameOperation userOperation(c);
	DocumentOperation docOperation(c);
	UserName n;
	n.name = "First";
	userOperation.insert(n);
	Document doc;
	docOperation.insert(doc);

	MultiPk pk;
	pk.documentId = doc.documentId;
	pk.nameId = n.userNameId;

	t.operation().insert(pk);
	BOOST_CHECK(true);
}

BOOST_AUTO_TEST_CASE( extends ) {
	Tester<ParseTask> t;
	auto c = t.connection();
	DocumentOperation docOperation(c);
	Document doc;
	docOperation.insert(doc);
	doc = Document();
	docOperation.insert(doc);
	BOOST_CHECK(!doc.documentId.isNull());

	ParseTask task;
	task.documentId = doc.documentId;
	t.operation().insert(task);
	BOOST_CHECK_EQUAL(doc.documentId , task.documentId);

	BOOST_CHECK(t.operation().loadTo(task));
}
