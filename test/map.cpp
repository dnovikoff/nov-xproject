#include "common_test.hpp"

#include <boost/algorithm/string.hpp>

#include <nov/xproject/generator/map.hpp>

using namespace Nov::XProject;

class TestObject {
public:
	std::string id;
};

typedef Map<TestObject> TestMap;

BOOST_AUTO_TEST_CASE ( doubleAdd ) {
	TestMap m;
	BOOST_CHECK(m.create("hi"));
	BOOST_CHECK(!m.create("hi"));
}

BOOST_AUTO_TEST_CASE ( getOfNotExisting ) {
	TestMap m;
	BOOST_CHECK(!m.get("id"));
}

BOOST_AUTO_TEST_CASE ( getOfExisting ) {
	TestMap m;
	m.create("hi");
	BOOST_CHECK(!m.get("id"));
}

BOOST_AUTO_TEST_CASE ( linkAfterAdd ) {
	TestMap m;
	m.create("hi");
	auto l = m.link("hi");
	BOOST_CHECK(l.isValid());
}

BOOST_AUTO_TEST_CASE ( linkBeforeAdd ) {
	TestMap m;
	auto l = m.link("hi");
	m.create("hi");
	BOOST_CHECK(l.isValid());
}

BOOST_AUTO_TEST_CASE ( linkOnNotExisting ) {
	TestMap m;
	auto l = m.link("hi");
	BOOST_CHECK(!l.isValid());
}

std::string testToString(const TestMap& m) {
	std::string ret;
	for (const auto& x: m.getOrdered()) {
		if (!ret.empty()) {
			ret += "|";
		}
		ret += x.id;
	}
	return ret;
}

BOOST_AUTO_TEST_CASE ( orderedTest ) {
	TestMap m;
	m.create("1");
	m.create("3");
	m.create("2");
	BOOST_CHECK_EQUAL(m.getOrdered().size(), 3u);
	BOOST_CHECK_EQUAL("1|3|2", testToString(m));
}

BOOST_AUTO_TEST_CASE ( nullLink ) {
	Link<int> l;
	BOOST_CHECK(!l.isValid());
}

