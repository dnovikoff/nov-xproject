#include "common_test.hpp"

#include <test/myproject/data/comment.hpp>
#include <test/myproject/data/object.hpp>
#include <test/myproject/data/user_name.hpp>
#include <test/myproject/meta/holder.hpp>

using namespace Test::Project;

template<typename T>
static std::string id2string(const Id<T>& x) {
	std::ostringstream ss;
	ss << x;
	return ss.str();
}

BOOST_AUTO_TEST_CASE ( testObjectName ) {
	auto holder = createMetaHolder();

	BOOST_CHECK_EQUAL(Comment::className(), "Comment");
	Comment comment;
	BOOST_CHECK_EQUAL(comment.className(), "Comment");
}

BOOST_AUTO_TEST_CASE ( testObjectId ) {
	auto holder = createMetaHolder();

	Comment comment;
	BOOST_CHECK_EQUAL(id2string(comment.objectId), "Id<Object>(NULL)");
}

BOOST_AUTO_TEST_CASE ( testNameId ) {
	auto holder = createMetaHolder();

	Comment comment;
	BOOST_CHECK_EQUAL(id2string(comment.nameId), "Id<UserName>(NULL)");
}
