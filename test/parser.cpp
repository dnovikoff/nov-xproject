#include "common_test.hpp"

#include <nov/xproject/generator/parser.hpp>

using namespace Nov::XProject;

bool parse(const std::string& x, Project& p) {
	try {
		Parser parser(p);
		parser.parse(x);
	} catch (const std::exception& e) {
		std::cout << e.what() << std::endl;
		return false;
	}
	return true;
}

bool parse(const std::string& x) {
	Project tmp;
	return parse(x, tmp);
}

BOOST_AUTO_TEST_CASE ( wrongType ) {
//	BOOST_CHECK(parse(""));
// TODO: CRASH HERE!
//	BOOST_CHECK(!parse("!type"));
	BOOST_CHECK(!parse("!type my"));
	BOOST_CHECK(!parse("!type my:"));
	BOOST_CHECK(!parse("!type my: []"));
	BOOST_CHECK(!parse("!type my: [!include]"));
	BOOST_CHECK(!parse("!type my: [!include '']"));

	BOOST_CHECK(!parse("!type my: [!include 'one', !include 'two']"));
	BOOST_CHECK(!parse("!type my: [!include 'one', !unknown 0]"));
	BOOST_CHECK(!parse("!type my: [!include 'one', !default 0, !default 1]"));
}

BOOST_AUTO_TEST_CASE ( correctType ) {
	BOOST_CHECK(parse("!type my: [!include 'one', !default 0]"));
	BOOST_CHECK(parse("!type my: [!include 'one']"));
}

BOOST_AUTO_TEST_CASE ( unknownTag ) {
	BOOST_CHECK(!parse("!hello world: 1"));
}

BOOST_AUTO_TEST_CASE ( correctClass ) {
	BOOST_CHECK(parse("!class Test:\n  one: [!type Integer]"));
	BOOST_CHECK(parse("!class Test:\n  !option extends: Other"));
}

BOOST_AUTO_TEST_CASE ( classWrong ) {
	BOOST_CHECK(!parse("!class Test: !wrong_tag extends: Other"));
	BOOST_CHECK(!parse("!class Test: !option wrong_option: Other"));
	BOOST_CHECK(!parse("!class Test: [!type WrongContent]"));
}

static const auto correctContent = R"(
!type Integer: [!include 'nov/database/types/integer.hpp', !default 0]
!type Timestamp: [!include 'nov/database/types/timestamp.hpp']
!type String: [!include 'nov/database/types/string.hpp']
!type Double: [!include 'nov/database/types/double.hpp', !default 0.0]
!type TestPerson: [!include 'test/project/test_person.hpp']


!class Document:
  httpCode: [!type Integer]
  timestamp: [!type Timestamp, !key index]
  url: [!type String]
  data: [!type String]

!class Object:
  parent: [!link Object]
  parsedFromDocument: [!link Document, !key index]

!class UserName:
  name: [!type String, !key unique]

!class Comment:
  !option extends: Object
  name: [!link UserName]
  originalTime: [!type Timestamp]

!class Parser:
  name: [!type String]

!class ParseTask:
  !option extends: Document
  startTime: [!type Timestamp]
  parser: [!link Parser]
  parentObject: [!link Object]
)";

BOOST_AUTO_TEST_CASE ( classCorrect ) {
	BOOST_CHECK(parse(correctContent));
}

static bool testValidate(Project& p) {
	try {
		Parser tmp(p);
		tmp.validate();
	} catch (const std::exception& e) {
		std::cout << e.what() << std::endl;
		return false;
	}
	return true;
}

BOOST_AUTO_TEST_CASE ( analyzeCorrect ) {
	Project p;
	BOOST_REQUIRE(parse(correctContent, p));
	BOOST_REQUIRE_EQUAL(p.types.getOrdered().size(), 5u);
	BOOST_REQUIRE_EQUAL(p.classes.getOrdered().size(), 6u);

	BOOST_CHECK(testValidate(p));
}
