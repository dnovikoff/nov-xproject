#ifndef TEST_PROJECT_HEADER_HPP_
#define TEST_PROJECT_HEADER_HPP_

#include <nov/database/types/id.hpp>
#include <nov/database/types/binary.hpp>
#include <nov/database/types/double.hpp>
#include <nov/database/types/integer.hpp>
#include <nov/database/types/enum.hpp>
#include <nov/database/types/optional.hpp>
#include <nov/database/types/timestamp.hpp>
#include <nov/database/types/string.hpp>

using Nov::Database::Types::String;
using Nov::Database::Types::Double;
using Nov::Database::Types::Integer;
using Nov::Database::Types::Timestamp;
using Nov::Database::Types::Id;

#endif // TEST_PROJECT_HEADER_HPP_
