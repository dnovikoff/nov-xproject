if(NOV_INCLUDE_xproject)
    return()
endif()

set(NOV_INCLUDE_xproject 1)

IF(NOT NOV_XPROJECT_DATA_DIR)
	SET(NOV_XPROJECT_DATA_DIR /usr/share/nov_xproject)
	SET(NOV_XPROJECT_LIB nov_xproject)
ELSE()
	SET(NOV_XPROJECT_LIB nov_xproject_shared)
ENDIF()

MACRO(NOV_XPROJECT_YAML name yaml)
	NOV_FILE(YAML_FILE ${yaml})
	# force cmake project regenerate
	# this is just to force regeneration
	# if yaml file changed
	CONFIGURE_FILE(${YAML_FILE} ${CMAKE_CURRENT_BINARY_DIR}/${name}.yaml)
	SET(OUT_DIR ${CMAKE_CURRENT_BINARY_DIR}/${name})

	MESSAGE("Generating '${name}' based on yaml file '${yaml}'")
	MESSAGE("YAML full path = '${YAML_FILE}'")
	MESSAGE("Output dir full path =  '${OUT_DIR}'")

	EXECUTE_PROCESS(
		COMMAND nov_xproject --yaml=${YAML_FILE} --output=${OUT_DIR}
		RESULT_VARIABLE result
	)

	IF(NOT "${result}" EQUAL "0")
		MESSAGE(FATAL_ERROR "Generation failed")
	ENDIF()

	FILE(READ "${OUT_DIR}/cmake_sources_list.txt" sources)

	set(lib_name ${name}_lib)
	ADD_LIBRARY(${lib_name} STATIC ${sources})
	SET_TARGET_PROPERTIES(${lib_name} PROPERTIES COMPILE_FLAGS ${BUILD_FLAGS} -fPIC)
	TARGET_LINK_LIBRARIES(${lib_name} ${NOV_XPROJECT_LIB})
ENDMACRO()
