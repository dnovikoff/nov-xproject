#include "generator.hpp"
#include "namespace.hpp"

namespace Nov {
namespace XProject {

void Generator::generateData(const Class& x) {
	generateDataHpp(x);
	generateDataCpp(x);
}

void Generator::generateDataCpp(const Class& x) {
	const auto name = classFileName(x);
	const auto metaClass = metaName(x);
	const auto dataClass = dataName(x);

	CppFile out(files.createFile("data/" + name + ".cpp"));
	out << "#include \"" << name << ".hpp\"" "\n\n";
	out << "#include <" << project.path << "/meta/" << name << ".hpp>" "\n\n";

	Namespace ns(out, project.ns);

	x.forEachField([this, &out, &dataClass](const Field& f) {
		out << "Nov::Database::BindedField " << dataClass << "::" << prefixedFieldName("bind", f) << "() {" "\n";
		out << "\t" "return meta()." << f.id << ".bind(" << f.id << ");" "\n";
		out << "}" "\n\n";
	});

	x.forEachField([this, &out, &dataClass](const Field& f) {
		out << "Nov::Database::AssignedField " << dataClass << "::" << prefixedFieldName("assign", f) << "() {" "\n";
		out << "\t" "return meta()." << f.id << ".assignRef(" << f.id << ");" "\n";
		out << "}" "\n\n";
	});

	out << "Nov::Database::BindedFields " << dataClass << "::bindAll() {" "\n";
	out << "\t" "return {" "\n";
	x.forEachField([this, &out, &dataClass](const Field& f) {
		out << "\t\t" << prefixedFieldName("bind", f) << "()," "\n";
	});
	out << "\t" "};" "\n";
	out << "}" "\n\n";

	out << "const " << metaClass << "& " << dataClass << "::meta() {" "\n";
	out << "\t" "return *" << metaClass << "::instance();" "\n";
	out << "}" "\n\n";

	out << "const std::string& " << dataClass << "::className() {" "\n";
	out << "\t" "return meta().getTable().name();" "\n";
	out << "}" "\n";
}

void Generator::generateDataHpp(const Class& x) {
	const auto name = classFileName(x);
	const auto p = "data/" + name + ".hpp";
	const auto metaClass = metaName(x);
	const auto dataClass = dataName(x);
	const auto operClass = operationName(x);
	HppFile out(files.createFile(p), createKeeper(p));
	out << "#include <string>" "\n";
	out << "#include <nov/database/field.hpp>" "\n\n";
	writeTypeIncludes(out, x);

	Namespace ns(out, project.ns);
	out << "class " << metaClass << ";" "\n";
	out << "class " << operClass << ";" "\n";
	out << "\n";
	out << "class " << dataClass << " {" "\n";
	out << "\t" "public:" "\n";
	out << "\t" "typedef " << metaClass << " Meta;" "\n";
	out << "\t" "typedef " << operClass << " Operation;" "\n";

	out << "\n";
	out << "// Fields" "\n";
	out << "// @{" "\n";
	x.forEachField([this, &out](const Field& f) {
		out << "\t" << fieldType(f) << " " << f.id;
		if (!f.type.isNull() && !f.type->def.empty()) {
			out << " = " << f.type->def;
		}
		out << ";" "\n";
	});
	out << "// @}" "\n";

	out << "\n";
	out << "// db bind helpers" "\n";
	out << "// @{" "\n";
	x.forEachField([this, &out](const Field& f) {
		out << "\t" "Nov::Database::BindedField " << prefixedFieldName("bind", f) << "();" "\n";
	});
	out << "// @}" "\n";

	out << "\n";
	out << "// db assign helpers" "\n";
	out << "// @{" "\n";
	x.forEachField([this, &out](const Field& f) {
		out << "\t" "Nov::Database::AssignedField " << prefixedFieldName("assign", f) << "();" "\n";
	});
	out << "// @}" "\n";

	out << "\n";
	out << "\t" "Nov::Database::BindedFields bindAll();" "\n";
	out << "\t" "static const std::string& className();" "\n";
	out << "\t" "static const Meta& meta();" "\n";
	out << "};" "\n";
}

} // namespace XProject
} // namespace Nov
