#ifndef NOV_XPROJECT_GENERATOR_PROJECT_HPP_
#define NOV_XPROJECT_GENERATOR_PROJECT_HPP_

#include <memory>
#include <vector>
#include <string>

#include <nov/xproject/generator/map.hpp>

namespace Nov {
namespace XProject {

class Type {
public:
	std::string id;
	std::string header;
	std::string def;
};

enum class Key {
	NotKey,
	Index,
	Unique,
	Primary,
	Slave,
	Auto
};

class Field {
public:
	std::string id;
	Link<Type> type;
	Link<class Class> link;
};

class KeyInfo {
public:
	// Unique
	std::string id;
	// In the order
	std::string name;
	std::map<std::string, Link<Field> > fields;
	Key key = Key::NotKey;
};

class Class {
public:
	std::string id;
	Map<Field> generatedFields;
	Map<Field> fields;

	KeyInfo primaryKey;
	Map<KeyInfo> keys;
	Map<KeyInfo> foreignKeys;

	bool autoInc = true;
	Link<Class> extends;

	template<typename T>
	void forEachField(T&& f) {
		auto f2 = generatedFields.forEach(std::move(f));
		fields.forEach(std::move(f2));
	}

	template<typename T>
	void forEachField(T&& f) const {
		auto f2 = generatedFields.forEach(std::move(f));
		fields.forEach(std::move(f2));
	}
};

class Project {
public:
	Map<Type> types;
	Map<Class> classes;

	std::vector<std::string> ns;
	std::string path;
};

} // namespace XProject
} // namespace Nov

#endif // NOV_XPROJECT_GENERATOR_PROJECT_HPP_
