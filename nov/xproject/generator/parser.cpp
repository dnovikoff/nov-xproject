#include "parser.hpp"

#include <stack>

#include <boost/algorithm/string/join.hpp>

#include <yaml-cpp/yaml.h>
#include <yaml-cpp/eventhandler.h>

namespace Nov {
namespace XProject {

#define NOV_LOCATION location(__FILE__, __LINE__)

static std::string firstToUpper(std::string x) {
	if (!x.empty()) {
		x[0] = std::toupper(x[0]);
	}
	return x;
}

class BaseHandler: public YAML::EventHandler {
public:
	void OnDocumentStart(const YAML::Mark& mark) override {
		handle("document start", mark);
	}
	void OnDocumentEnd() {
	}
	void OnNull(const YAML::Mark& mark, YAML::anchor_t) override {
		handle("null", mark);
	}
	void OnAlias(const YAML::Mark& mark, YAML::anchor_t) override {
		handle("alias", mark);
	}
	void OnScalar(const YAML::Mark& mark, const std::string& tag, YAML::anchor_t, const std::string& value) override {
		handle("scalar !" + tag + " " + value , mark);
	}
	void OnSequenceStart(const YAML::Mark& mark, const std::string& tag, YAML::anchor_t) override {
		handle("sequence !" + tag, mark);
	}
	void OnSequenceEnd() override {
	}
	void OnMapStart(const YAML::Mark& mark, const std::string& tag, YAML::anchor_t) override {
		handle("map !" + tag, mark);
	}
	void OnMapEnd() override {
	}

protected:
	std::string where(const YAML::Mark& mark) {
		std::ostringstream oss;
		oss << "pos " << mark.pos+1 << ", line " << mark.line+1 << ", column " << mark.column+1;
		return oss.str();
	}

	std::string location(const std::string& file, int line) {
		std::ostringstream oss;
		oss << ": " << file << ": " << line;
		return oss.str();
	}

	void finished() {
		done = true;
	}

	template<typename T, typename... Args>
	void pushNext(Args&&... args) {
		next = std::unique_ptr<T>(new T(std::forward<Args>(args)...));
	}

private:
	void handle(const std::string& what, const YAML::Mark& mark) {
		std::ostringstream oss;
		oss << "Unexpected " << what << " at " << where(mark);
		throw std::runtime_error(oss.str());
	}

	friend class StateHandler;

	bool done = false;
	std::unique_ptr<BaseHandler> next;
};

class SequenceHandler: public BaseHandler {
public:
	void OnSequenceStart(const YAML::Mark& mark, const std::string& tag, YAML::anchor_t) override {
		started = true;
		if (!tag.empty()) {
			throw std::runtime_error("Tag " + tag + " is not expected at " + where(mark) + NOV_LOCATION);
		}
	}
	virtual void onValue(const YAML::Mark& mark, const std::string& tag, const std::string& value) = 0;
	virtual void onDone() {}
	void OnScalar(const YAML::Mark& mark, const std::string& tag, YAML::anchor_t, const std::string& value) override {
		if (!started) {
			throw std::runtime_error("Expected sequence at " + where(mark));
		}
		onValue(mark, tag, value);
	}

	void OnSequenceEnd() override {
		onDone();
		finished();
	}

private:
	bool started = false;
};

class TypeHandler: public SequenceHandler {
public:
	explicit TypeHandler(Type& t): type(t) {
	}

	void onValue(const YAML::Mark& mark, const std::string& tag, const std::string& value) override {
		if (tag == "!include") {
			if (!type.header.empty()) {
				throw std::runtime_error("Duplicate header value at " + where(mark));
			}
			type.header = value;
		} else if (tag == "!default") {
			if (!type.def.empty()) {
				throw std::runtime_error("Duplicate default value at " + where(mark));
			}
			type.def = value;
		} else {
			throw std::runtime_error("Unexpected tag '" + tag + "' at " + where(mark) + NOV_LOCATION);
		}
	}

	void onDone() override {
		if (type.header.empty()) {
			throw std::runtime_error("Type '" + type.id + "' expected to have '!include'");
		}
	}

private:
	Type& type;
};

class NamespaceHandler: public SequenceHandler {
public:
	explicit NamespaceHandler(std::vector<std::string>& n): ns(n) {
	}

	void onValue(const YAML::Mark& mark, const std::string& tag, const std::string& value) override {
		if (!tag.empty()) {
			throw std::runtime_error("Unexpected tag '" + tag + "' at " + where(mark) + NOV_LOCATION);
		}
		ns.push_back(value);
	}

	void onDone() override {
		if (ns.empty()) {
			throw std::runtime_error("Namespace could not be empty");
		}
	}

private:
	std::vector<std::string>& ns;
};

class FieldHandler: public SequenceHandler {
public:
	FieldHandler(Project& p, Class& c, Field& f): project_(p), class_(c), field_(f) {
	}

	void onValue(const YAML::Mark& mark, const std::string& tag, const std::string& value) override {
		if (tag == "!key") {
			if (keyType_ != Key::NotKey) {
				throw std::runtime_error("Key is already set for field '" + field_.id + "' at " + where(mark));
			} else if (value == "index") {
				keyType_ = Key::Index;
			} else if (value == "unique") {
				keyType_ = Key::Unique;
			} else {
				throw std::runtime_error("Unexpected key type '" + value + "'. Expected 'unique' or 'index' at " + where(mark) + NOV_LOCATION);
			}
		} else if (tag == "!type") {
			if (!field_.link.isNull()) {
				throw std::runtime_error("Unexpected type '" + value + "' (link is already set) at " + where(mark) + NOV_LOCATION);
			}
			field_.type = project_.types.link(value);
		} else if (tag == "!link") {
			if (!field_.type.isNull()) {
				throw std::runtime_error("Unexpected link '" + value + "' (type is already set) at " + where(mark) + NOV_LOCATION);
			}
			field_.link = project_.classes.link(value);
		} else {
			throw std::runtime_error("Unexpected tag '" + tag + "' at " + where(mark) + NOV_LOCATION);
		}
	}

	void addKey() {
		if (keyType_ == Key::NotKey) {
			return;
		}

		auto info = class_.keys.create(firstToUpper(field_.id));
		if (!info) {
			throw std::runtime_error("Error adding key '" + field_.id + "' " + NOV_LOCATION);
		}
		info->name = info->id;
		info->key = keyType_;
		info->fields[field_.id] = class_.fields.link(field_.id);
	}

	void onDone() override {
		addKey();
	}

private:
	Key keyType_ = Key::NotKey;

	Project& project_;
	Class& class_;
	Field& field_;
};

class KeysHandler: public SequenceHandler {
public:
	explicit KeysHandler(Class& c, Key k): class_(c) {
		info_.key = k;
	}

	void onValue(const YAML::Mark& mark, const std::string& tag, const std::string& value) override {
		if (!tag.empty()) {
			throw std::runtime_error("Tags unexpected for key at " + where(mark));
		}
		auto x = class_.fields.link(value);
		if (!x.isValid()) {
			throw std::runtime_error("Unknown field '" + value + "' at " + where(mark));
		}
		if (info_.fields.count(value)) {
			throw std::runtime_error("Duplicate field '" + value + "' for key at " + where(mark));
		}
		info_.fields[value] = x;
	}

	void onDone() override {
		std::set<std::string> ids;
		std::string name;
		for (const auto& p: info_.fields) {
			auto& id = p.second.id();
			ids.insert(firstToUpper(id));
			name += firstToUpper(id);
		}

		const std::string id = boost::join(ids, "");

		if (info_.fields.size() < 1u) {
			throw std::runtime_error("Expected key '" + id + "' for class '" + class_.id + "' to have at least one field");
		}
		info_.id = id;
		info_.name = name;

		if (info_.key == Key::Primary) {
			if (!class_.primaryKey.id.empty()) {
				throw std::runtime_error("Duplicate primary key for class '" + class_.id + "'");
			}
			class_.primaryKey = info_;
			return;
		}
		auto x = class_.keys.create(id, info_);
		if (!x || class_.primaryKey.id == info_.id) {
			throw std::runtime_error("Duplicate key '" + id + "' for class '" + class_.id + "'");
		}
	}

private:
	Class& class_;
	KeyInfo info_;
};

class StringHandler: public BaseHandler {
public:
	explicit StringHandler(std::string& s): str(s) {
	}

	void OnScalar(const YAML::Mark& mark, const std::string& tag, YAML::anchor_t, const std::string& value) override {
		if (!tag.empty()) {
			throw std::runtime_error("Unexpected tag '" + tag + "' at " + where(mark) + NOV_LOCATION);
		}
		str = value;
		finished();
	}

private:
	std::string& str;
};

class ClassHandler: public BaseHandler {
public:
	explicit ClassHandler(Class& c, Project& p): classValue(c), project(p) {
	}
	void OnMapStart(const YAML::Mark& mark, const std::string& tag, YAML::anchor_t) override {
		if (!tag.empty()) {
			throw std::runtime_error("Tag '" + tag + "' is not expected at " + where(mark));
		}
	}
	void OnScalar(const YAML::Mark& mark, const std::string& tag, YAML::anchor_t, const std::string& value) override {
		if (state == Extends) {
			if (!tag.empty()) {
				throw std::runtime_error("Unexpected tag '" + tag + "' at " + where(mark) + NOV_LOCATION);
			}
			if (!classValue.extends.isNull()) {
				throw std::runtime_error("Extends option already redefined at " + where(mark) + NOV_LOCATION);
			}
			classValue.extends = project.classes.link(value);
			state = Normal;
		} else if (tag == "!option") {
			if (value == "extends") {
				state = Extends;
			} else {
				throw std::runtime_error("Unexpected class option '" + value + "' at " + where(mark) + NOV_LOCATION);
			}
		} else if (tag == "!key") {
			Key key {Key::NotKey};
			if (value == "index") {
				key = Key::Index;
			} else if (value == "unique") {
				key = Key::Unique;
			} else if (value == "primary") {
				key = Key::Primary;
			} else {
				throw std::runtime_error("Unexpected key type '" + value + "' at " + where(mark) + NOV_LOCATION);
			}
			pushNext<KeysHandler>(classValue, key);
		} else if (!tag.empty()) {
			throw std::runtime_error("Unexpected tag '" + tag + "' at " + where(mark) + NOV_LOCATION);
		} else {
			auto p = classValue.fields.create(value);
			if (!p) {
				throw std::runtime_error("Duplicate field '" + value + "' at " + where(mark) + NOV_LOCATION);
			}
			pushNext<FieldHandler>(project, classValue, *p);
		}
	}
	void OnMapEnd() override {
		finished();
	}

private:
	enum State {
		Normal,
		Extends,
	};
	State state = Normal;
	Class& classValue;
	Project& project;
};

class ProjectHandler: public BaseHandler {
public:
	explicit ProjectHandler(Project& p): project(p) {
	}

	void OnDocumentStart(const YAML::Mark&) override {
		if (started) {
			throw std::runtime_error("Only one document expected" + NOV_LOCATION);
		}
		started = true;
	}
	void OnMapStart(const YAML::Mark& mark, const std::string& tag, YAML::anchor_t) override {
		mapStarted = true;
		if (!tag.empty()) {
			throw std::runtime_error("Tag '" + tag + "' is not expected at " + where(mark) + NOV_LOCATION);
		}
	}
	void OnScalar(const YAML::Mark& mark, const std::string& tag, YAML::anchor_t, const std::string& value) override {
		if (!mapStarted) {
			throw std::runtime_error("Map expected at " + where(mark) + NOV_LOCATION);
		}
		if (tag == "!option") {
			if (value == "namespace") {
				if (!project.ns.empty()) {
					throw std::runtime_error("Redefining namespace at " + where(mark) + NOV_LOCATION);
				}
				pushNext<NamespaceHandler>(project.ns);
			} else if (value == "path") {
				if (!project.path.empty()) {
					throw std::runtime_error("Redefining path at " + where(mark) + NOV_LOCATION);
				}
				pushNext<StringHandler>(project.path);
			} else {
				throw std::runtime_error("Unexpected key '" + value + "' at " + where(mark) + NOV_LOCATION);
			}
		} else if (tag == "!type") {
			auto t = project.types.create(value);
			if (!t) {
				throw std::runtime_error("Duplicate type '" + value + "' at " + where(mark) + NOV_LOCATION);
			}
			pushNext<TypeHandler>(*t);
		} else if (tag == "!class") {
			auto t = project.classes.create(value);
			if (!t) {
				throw std::runtime_error("Duplicate class '" + value + "' at " + where(mark) + NOV_LOCATION);
			}
			pushNext<ClassHandler>(*t, project);
		} else {
			throw std::runtime_error("Tag '" + tag + "' is not expected at " + where(mark) + NOV_LOCATION);
		}
	}
	void OnDocumentEnd() {
		if (!mapStarted) {
			throw std::runtime_error("Map expected" + NOV_LOCATION);
		}
	}

private:
	Project& project;
	bool started = false;
	bool key = true;
	bool mapStarted = false;
};

static std::string emptyString;

const std::string& fixTag(const std::string& tag) {
	return (tag == "?"?emptyString:tag);
}

class StateHandler: public YAML::EventHandler {
public:
	template<typename T, typename... Args>
	void push(Args&&... args) {
		stack.push(std::unique_ptr<BaseHandler>(new T(std::forward<Args>(args)...)));
	}

	void OnDocumentStart(const YAML::Mark& mark) override {
		top().OnDocumentStart(mark);
	}
	void OnDocumentEnd() {
		top().OnDocumentEnd();
	}
	void OnNull(const YAML::Mark& mark, YAML::anchor_t a) override {
		top().OnNull(mark, a);
	}
	void OnAlias(const YAML::Mark& mark, YAML::anchor_t a) override {
		top().OnAlias(mark, a);
	}
	void OnScalar(const YAML::Mark& mark, const std::string& tag, YAML::anchor_t a, const std::string& value) override {
		top().OnScalar(mark, fixTag(tag), a, value);
	}
	void OnSequenceStart(const YAML::Mark& mark, const std::string& tag, YAML::anchor_t a) override {
		top().OnSequenceStart(mark, fixTag(tag), a);
	}
	void OnSequenceEnd() override {
		top().OnSequenceEnd();
	}
	void OnMapStart(const YAML::Mark& mark, const std::string& tag, YAML::anchor_t a) override {
		top().OnMapStart(mark, fixTag(tag), a);
	}
	void OnMapEnd() override {
		top().OnMapEnd();
	}

private:
	BaseHandler& top() {
		auto& x = *stack.top();
		if (x.done) {
			stack.pop();
			return top();
		} else if (x.next) {
			stack.push(std::move(x.next));
			return top();
		}
		return x;
	}

	std::stack<std::unique_ptr<BaseHandler> > stack;
};

void Parser::parse(const std::string& content) {
	std::istringstream oss(content);

	YAML::Parser parser(oss);

	StateHandler handler;
	handler.push<ProjectHandler>(project);
	YAML::Node doc;
	while (parser.HandleNextDocument(handler)) {
	}
}

void Parser::validate() {
	project.classes.forEach([] (Class& x) {
		if (!x.extends.isNull() && !x.extends.isValid()) {
			throw std::runtime_error("Class " + x.id + " extends undefined class " + x.extends.id());
		}
		x.fields.forEach([&x] (Field& f) {
			if (!f.type.isNull()) {
				if (!f.type.isValid()) {
					throw std::runtime_error(x.id + "." + f.id + " points to undefined type " + f.type.id());
				}
			} else if (!f.link.isValid()) {
				throw std::runtime_error(x.id + "." + f.id + " points to undefined class " + f.link.id());
			}
		});
	});
}

} // namespace XProject
} // namespace Nov
