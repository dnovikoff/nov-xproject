#ifndef NOV_XPROJECT_GENERATOR_PARSER_HPP_
#define NOV_XPROJECT_GENERATOR_PARSER_HPP_

#include <nov/xproject/generator/project.hpp>

namespace Nov {
namespace XProject {

class Parser {
public:
	explicit Parser(Project& p): project(p) {
	}

	void parse(const std::string& content);

	void validate();

private:
	Project& project;
};

} // namespace XProject
} // namespace Nov

#endif // NOV_XPROJECT_GENERATOR_PARSER_HPP_
