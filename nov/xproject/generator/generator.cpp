#include "generator.hpp"
#include "parser.hpp"

#include <fstream>
#include <iostream>

#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

namespace Nov {
namespace XProject {

static std::string firstToLower(std::string x) {
	if (!x.empty()) {
		x[0] = std::tolower(x[0]);
	}
	return x;
}

bool Generator::init(int argc, char **argv) {
	namespace po = boost::program_options;
	po::options_description commands("Allowed options");

	commands.add_options()
		("help", "print help")
		("yaml", po::value<std::string>(&input), "Path to input yaml file")
		("output", po::value<std::string>(&output), "Path to output dir")
	;

	po::variables_map vm;
	po::store(parse_command_line(argc, argv, commands, po::command_line_style::unix_style ^ po::command_line_style::allow_short), vm);
	po::notify(vm);

	bool doExit = false;
	if (vm.count("help")) {
		doExit = true;
	} else if (input.empty()) {
		std::cerr << "--yaml should be set" << std::endl;
		doExit = true;
	} else if (output.empty()) {
		std::cerr << "--output should be set" << std::endl;
		doExit = true;
	}

	if (doExit) {
		std::cerr << commands << std::endl;
		return false;
	}
	return true;
}

void Generator::run() {
	read();
	generate();
	files.saveFiles(output);
}

void Generator::generate() {
	for (const auto& x: project.classes.getOrdered()) {
		generateOperation(x);
		generateMeta(x);
		generateData(x);
		generateHeader(x);
	}

	generateMetaHolder();
	generateFileList();
}

std::string Generator::classFileName(const Class& c) {
	std::string result;
	result.reserve(c.id.length()*2);
	bool prevIsUpper = true;
	for (const auto& x: c.id) {
		auto l = std::tolower(x);
		if (l != x) {
			if (!prevIsUpper) {
				result += '_';
				prevIsUpper = true;
			}
		} else {
			prevIsUpper = false;
		}
		result += l;
	}
	return result;
}

std::string Generator::createKeeper(const std::string& path) {
	std::string result(project.path + "_" + path + '_');
	for (auto& x: result) {
		if (std::isalnum(x)) {
			x = std::toupper(x);
		} else {
			x = '_';
		}
	}
	return result;
}

std::string Generator::metaName(const Class& x) {
	return x.id + "Meta";
}

std::string Generator::dataName(const Class& x) {
	return x.id;
}

std::string Generator::operationName(const Class& x) {
	return x.id + "Operation";
}

std::string Generator::fixedFieldName(const Field& x) {
	if (x.link.isNull()) {
		return x.id;
	}
	return x.id + "Id";
}

std::string Generator::prefixedFieldName(const std::string& prefix, const Field& x) {
	auto name = x.id;
	if (!name.empty()) {
		name[0] = std::toupper(name[0]);
	}
	return prefix + name;
}

std::string Generator::fieldType(const Field& x) {
	if (x.link.isNull()) {
		return x.type.id();
	}
	return "Nov::Database::Types::Id<class " + x.link->id + ">";
}

std::string Generator::classNameToId(const Class& x) {
	std::string ret(x.id);
	if (!ret.empty()) {
		ret[0] = std::tolower(ret[0]);
	}
	return ret + "Id";
}

std::string Generator::classType(const Class& x) {
	return "Nov::Database::Id<class " + x.id + ">";
}

void Generator::writeIncludes(
	CppFile& file, const Class& x,
	const std::string& prefix, const std::string& suffix
) {
	std::set<std::string> files;
	x.forEachField([this, &files, &x](const Field& f) {
		if (f.link.isNull() || f.link->id == x.id) {
			return;
		}
		files.insert(classFileName(*f.link));
	});
	for (const auto& f: files) {
		file << "#include <" << prefix << f << suffix << ">\n";
	}
}

void Generator::writeHppIncludes(CppFile& file, const Class& x, const std::string& folder) {
	writeIncludes(file, x, project.path + "/" + folder + "/", ".hpp");
}

void Generator::writeDataIncludes(CppFile& file, const Class& x) {
	writeHppIncludes(file, x, "data");
}

void Generator::writeMetaIncludes(CppFile& file, const Class& x) {
	writeHppIncludes(file, x, "meta");
}
void Generator::writeOperationIncludes(CppFile& file, const Class& x) {
	writeHppIncludes(file, x, "operation");
}

void Generator::writeTypeIncludes(CppFile& file, const Class& x) {
	std::set<std::string> includes;
	x.forEachField([this, &includes, &x](const Field& f) {
		if (f.type.isNull()) {
			return;
		}
		includes.insert(f.type->header);
	});
	for (const auto& i: includes) {
		file << "#include <" << i << ">" "\n";
	}
	if (!includes.empty()) {
		file << "\n";
	}

	file << "#include <nov/database/types/id.hpp>" "\n\n";
}

void Generator::read() {
	Parser parser(project);
	parser.parse(files.readFile(input));
	parser.validate();

	project.classes.forEach([this](Class& x) {
		x.fields.forEach([this](Field& f) {
			f.id = fixedFieldName(f);
		});
		x.fields.remap();

		KeyInfo& info = x.primaryKey;

		if (!x.extends.isNull()) {
			// Slave
			if (!info.fields.empty()) {
				throw std::runtime_error("Could not create slave key for '" + x.id + "'");
			}
			info = x.extends->primaryKey;
			info.key = Key::Slave;
			x.generatedFields = x.extends->generatedFields;
			x.foreignKeys.create("_automatic_", info);
		} else if (info.fields.empty()) {
			// Automatic primary key
			Field* primaryField = x.generatedFields.create(firstToLower(x.id) + "Id");
			if (!primaryField) {
				throw std::runtime_error("Could not create primary field for class '" + x.id + "'");
			}
			// self link
			primaryField->link = project.classes.link(x.id);
			info.key = Key::Auto;
			info.fields[primaryField->id] = x.generatedFields.link(primaryField->id);
		}

		// This is for foreign keys generation
		x.fields.forEach([this, &x](Field& f) {
			if (f.link.isNull()) {
				return;
			}
			auto key = x.foreignKeys.create(f.id);
			key->name = key->id;
			key->key = Key::Index;
			key->fields[f.id] = Link<Field>(&f);
		});
	});
}

void Generator::generateFileList() {
	CppFile meta(files.createFile("meta.cpp"));
	meta << "#include \"meta/holder.cpp\"" "\n";
	CppFile oper(files.createFile("operation.cpp"));
	CppFile data(files.createFile("data.cpp"));
	auto list = files.createFile("cmake_sources_list.txt");
	list << output << "/meta/holder.cpp";
	for (const auto& x: project.classes.getOrdered()) {
		const auto name = classFileName(x);
		meta << "#include \"meta/" << name << ".cpp\"" "\n";
		oper << "#include \"operation/" << name << ".cpp\"" "\n";
		data << "#include \"data/" << name << ".cpp\"" "\n";

		list << ";" << output << "/meta/" << name << ".cpp";
		list << ";" << output << "/operation/" << name << ".cpp";
		list << ";" << output << "/data/" << name << ".cpp";
	}
}

void Generator::generateHeader(const Class& x) {
	const auto name = classFileName(x) + ".hpp";
	HppFile out(files.createFile(name), createKeeper(name));
	out << "#include <" << project.path << "/meta/" << name << ">" "\n";
	out << "#include <" << project.path << "/operation/" << name << ">" "\n";
	out << "#include <" << project.path << "/data/" << name << ">" "\n";
}

} // namespace XProject
} // namespace Nov
