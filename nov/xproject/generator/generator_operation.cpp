#include "generator.hpp"
#include "namespace.hpp"

namespace Nov {
namespace XProject {

void Generator::generateOperation(const Class& x) {
	generateOperationHpp(x);
	generateOperationCpp(x);
}

void addAferComma(std::string& x, const std::string& text) {
	if (!x.empty()) {
		x += ", ";
	}
	x += text;
}

struct StringsForFields {
public:
	std::string constRefs;
	std::string names;
	std::string refs;
	std::string map;
	std::string assignCopy;
	std::string assign;
	std::string toObject;

	void add(const Field& x) {
		addAferComma(constRefs, "const " + Generator::fieldType(x) + "& " + x.id);
		addAferComma(refs, x.type.id() + "& " + x.id);
		addAferComma(names, x.id);
		addAferComma(map, "FieldInfo(\"" + x.id + "\", obj." + x.id + ")");
		toObject += "\t" "obj." + x.id + " = "  + x.id + ";\n";
		addAferComma(assign, "\n\t\t" "obj." + Generator::prefixedFieldName("assign", x) + "()");
		addAferComma(assignCopy, "\n\t\t" "meta()." + x.id + ".assignCopy(" + x.id + ")");
	}
};

static StringsForFields createStrings(const std::map<std::string, Link<Field> >& fields) {
	StringsForFields sff;
	for (const auto& p: fields) {
		sff.add(*p.second);
	}
	return sff;
}

void Generator::generateOperationCpp(const Class& x) {
	const auto name = classFileName(x);
	const auto metaClass = metaName(x);
	const auto dataClass = dataName(x);
	const auto operClass = operationName(x);

	CppFile out(files.createFile("operation/" + name + ".cpp"));
	out << "#include \"" << name << ".hpp\"" "\n\n";
	out << "#include <nov/database/result.hpp>" "\n";
	out << "#include <nov/xproject/exception.hpp>" "\n";
	out << "\n";
	out << "#include <" << project.path << "/meta/" << name << ".hpp>" "\n";
	out << "#include <" << project.path << "/data/" << name << ".hpp>" "\n";
	writeDataIncludes(out, x);
	out << "\n";
	Namespace ns(out, project.ns);

	out << "using Nov::Database::AssignedFields;" "\n";
	out << "using Nov::Database::BindedFields;" "\n";
	out << "using Nov::XProject::throwObjectNotFound;" "\n";
	out << "using Nov::XProject::FieldInfo;" "\n";
	out << "using Nov::XProject::FieldInfos;" "\n";

	out << "\n";

	out << operClass << "::" << operClass << "(const std::shared_ptr<Nov::Database::Connection>& c): operation(meta(), c) {" "\n";
	out << "}" "\n\n";

	// Insert
	out << "void " << operClass << "::insert(" << dataClass << "& obj) {" "\n";
	out << "\t" "AssignedFields input {" "\n";

	for (const auto& f: x.fields.getOrdered()) {
		out << "\t\t" "obj." << prefixedFieldName("assign", f) << "()," "\n";
	}
	if (!x.extends.isNull()) {
		for (const auto& p: x.primaryKey.fields) {
			auto& f = *p.second;
			out << "\t\t" "obj." << prefixedFieldName("assign", f) << "()," "\n";
		}
	}
	out << "\t" "};" "\n";

	if (!x.extends.isNull() && x.primaryKey.key == Key::Auto) {
		for (const auto& p: x.primaryKey.fields) {
			auto& f = *p.second;
			out << "\t" "if (!obj." << f.id << ".isNull()) {" "\n";
			out << "\t\t" "input.push_back(obj." << prefixedFieldName("assign", f) << "());" "\n";
			out << "\t" "}" "\n";
		}
	}

	out << "\t" "operation.insert(input";
	if (x.primaryKey.key == Key::Auto) {
		out << ", obj." << x.primaryKey.fields.begin()->second.id();
	}
	out << ");" "\n";
	out << "}" "\n\n";

	auto primaryStrings = createStrings(x.primaryKey.fields);

	// load by primary key
	out << dataClass << " " << operClass << "::load(" << primaryStrings.constRefs << ") {" "\n";
	out << "\t" << dataClass << " obj;" "\n";
	out << primaryStrings.toObject;
	out << "\t" "if (!loadTo(obj)) {" "\n";
	out << "\t\t" "throwObjectNotFound(" << dataClass << "::className(), FieldInfos{" << primaryStrings.map << "});" "\n";
	out << "\t" "}" "\n";
	out << "\t" "return obj;" "\n";
	out << "}" "\n\n";

	// load by ref
	out << "bool " << operClass << "::loadTo(" << dataClass << "& obj) {" "\n";
	out << "\t" "const BindedFields output {" "\n";
	for (const auto& f: x.fields.getOrdered()) {
		out << "\t\t" "obj." << prefixedFieldName("bind", f) << "()," "\n";
	}
	out << "\t" "};" "\n";
	out << "\t" "const AssignedFields where {";
	out << primaryStrings.assign << "\n";
	out << "\t" "};" "\n";
	out << "\t" "return operation.load(output, where).fetch();" "\n";
	out << "}" "\n\n";

	out << "void " << operClass << "::remove(" << dataClass << "& obj) {" "\n";
	out << "\t" "const AssignedFields where {";
	out << primaryStrings.assign << "\n";
	out << "\t" "};" "\n";
	out << "\t" "if (0u == operation.remove(where)) {" "\n";
	out << "\t\t" "throwObjectNotFound(" << dataClass << "::className(), FieldInfos{" << primaryStrings.map << "});" "\n";
	out << "\t" "}" "\n";
	out << "}" "\n\n";

	// Remove
	out << "void " << operClass << "::remove(" << primaryStrings.constRefs << ") {" "\n";
	out << "\t" << dataClass << " obj;" "\n";
	out << primaryStrings.toObject << "\n";
	out << "\t" "remove(obj);" "\n";
	out << "}" "\n\n";

	// Update
	out << "bool " << operClass << "::update(" << dataClass << "& obj, " << dataClass << "& newState) {" "\n";
	out << "\t" "AssignedFields fields;" "\n";
	out << "\t" "fields.reserve(" << x.fields.getOrdered().size() +1 << "u);" "\n";
	out << "\n";
	x.forEachField([&out, this] (const Field& f) {
		out << "\t" "if (obj." << f.id << " != newState." << f.id << ") {" "\n";
		out << "\t\t" "fields.push_back(newState." << prefixedFieldName("assign", f) << "());" "\n";
		out << "\t" "}" "\n";
	});
	out << "\n";
	out << "\t" "if (fields.empty()) {" "\n";
	out << "\t\t" "return false;" "\n";
	out << "\t" "}" "\n";
	out << "\n";

	out << "\t" "const AssignedFields where {";
	out << primaryStrings.assign << "\n";
	out << "\t" "};" "\n";

	out << "\t" "if (0u == operation.update(fields, where)) {" "\n";
	out << "\t\t" "throwObjectNotFound(" << dataClass << "::className(), FieldInfos{" << primaryStrings.map << "});" "\n";
	out << "\t" "}" "\n";
	out << "\t" "return true;" "\n";

	out << "}" "\n\n";

	// SELECT *
	out << "Nov::Database::BindedResult " << operClass << "::selectAllTo(" << dataClass << "& obj) {" "\n";
	out << "\t" "return operation.load(obj.bindAll(), {});" "\n";
	out << "}" "\n\n";

	for (const auto& info: x.keys.getOrdered()) {
		const auto keyStrings = createStrings(info.fields);
		{
			const auto name = "selectBy" + info.name + "To";
			out << "Nov::Database::BindedResult " << operClass << "::" << name << "(" << dataClass << "& obj) {" "\n";
			out << "\t" "const BindedFields output {" "\n";
			x.forEachField([&out, this, &info] (const Field& f2) {
				if (info.fields.count(f2.id)) {
					return;
				}
				out << "\t\t" "obj." << prefixedFieldName("bind", f2) << "()," "\n";
			});
			out << "\t" "};" "\n\n";
			out << "\t" "const AssignedFields where {";
			out << keyStrings.assign << "\n";
			out << "\t" "};" "\n\n";
			out << "\t" "return operation.load(output, where);" "\n";
			out << "}" "\n\n";
		}

		{
			const auto name = "existsBy" + info.name;
			out << "bool " << operClass << "::" << name << "(" << dataClass << "& obj) {" "\n";
			out << "\t" "const AssignedFields where {";
			out << keyStrings.assign << "\n";
			out << "\t" "};" "\n\n";
			out << "\t" "return operation.exists(where);" "\n";
			out << "}" "\n\n";
		}

		if (info.key == Key::Unique) {
			const auto name = "loadBy" + info.name;
			out << "bool " << operClass << "::" << name << "To(" << dataClass << "& obj) {" "\n";
			out << "\t" "return selectBy" << info.id << "To(obj).fetch();" "\n";
			out << "}" "\n\n";

			out << dataClass << " " << operClass << "::" << name << "(" << keyStrings.constRefs << ") {" "\n";
			out << "\t" << dataClass << " obj;" "\n";
			out << keyStrings.toObject << "\n";
			out << "\t" " if (!" + name+ "To(obj)) {" "\n";
			out << "\t\t" "throwObjectNotFound(" << dataClass << "::className(), FieldInfos{" << keyStrings.map << "});" "\n";
			out << "\t" " }" "\n";
			out << "\t" "return obj;" "\n";
			out << "}" "\n\n";
		}
	}

	out << "const " << metaClass << "& " << operClass << "::meta() {" "\n";
	out << "\t" "return " << dataClass << "::meta();" "\n";
	out << "}" "\n\n";
}

void Generator::generateOperationHpp(const Class& x) {
	const auto name = classFileName(x);
	const auto p = "operation/" + name + ".hpp";
	const auto metaClass = metaName(x);
	const auto dataClass = dataName(x);
	const auto operClass = operationName(x);

	const auto primaryStrings = createStrings(x.primaryKey.fields);

	HppFile out(files.createFile(p), createKeeper(p));

	out << "#include <memory>" "\n";
	out << "#include <nov/database/connection.hpp>" "\n";
	out << "#include <nov/database/operation.hpp>" "\n";
	writeTypeIncludes(out, x);
	out << "\n";

	Namespace ns(out, project.ns);
	out << "class " << metaClass << ";" "\n";
	out << "class " << dataClass << ";" "\n";
	out << "\n";
	out << "class " << operClass << " {" "\n";
	out << "public:" "\n";
	out << "\t" "explicit " << operClass << "(const std::shared_ptr<Nov::Database::Connection>& c);" "\n";
	out << "\t" << operClass << "(" << operClass << "&&) = default;" "\n";
	out << "\t" << operClass << "& operator=(" << operClass << "&&) = default;" "\n";
	out << "\n";
	out << "\t" "void insert(" << dataClass << "&);" "\n";
	out << "\t" << dataClass << " load(" << primaryStrings.constRefs << ");" "\n";
	out << "\t" "bool loadTo(" << dataClass << "&);" "\n";
	out << "\t" "void remove(" << dataClass << "&);" "\n";
	out << "\t" "void remove(" << primaryStrings.constRefs << ");" "\n";
	out << "\t" "bool update(" << dataClass << "& oldState, " << dataClass << "& newState);" "\n";
	out << "\n";
	out << "\t" "Nov::Database::BindedResult selectAllTo(" << dataClass << "& obj);" "\n";

	for (const auto& info: x.keys.getOrdered()) {
		const auto name = "selectBy" + info.name + "To";
		out << "\t" "Nov::Database::BindedResult " << name << "(" << dataClass << "& obj);" "\n";
		out << "\t" "bool existsBy" << info.name << "(" << dataClass << "&);" "\n";

		if (info.key == Key::Unique) {
			auto keyStrings = createStrings(info.fields);
			const auto name = "loadBy" + info.name;
			out << "\t" "bool " << name << "To(" << dataClass << "&);" "\n";
			out << "\t" << dataClass << " " << name << "(" << keyStrings.constRefs << ");" "\n";
		}
	}
	out << "private:" "\n";
	out << "\t" "static const " << metaClass << "& meta();" "\n";
	out << "\n";
	out << "\t" "Nov::Database::Operation operation;" "\n";
	out << "};" "\n";
}

} // namespace XProject
} // namespace Nov
