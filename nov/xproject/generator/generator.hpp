#ifndef NOV_XPROJECT_GENERATOR_GENERATOR_HPP_
#define NOV_XPROJECT_GENERATOR_GENERATOR_HPP_

#include <string>
#include <map>
#include <nov/xproject/generator/project.hpp>
#include <nov/xproject/generator/file.hpp>

namespace Nov {
namespace XProject {

class Generator {
public:
	bool init(int argc, char **argv);
	void run();
	void generate();

	std::string classFileName(const Class& c);
	std::string createKeeper(const std::string& path);
	std::string metaName(const Class& x);
	std::string dataName(const Class& x);
	std::string operationName(const Class& x);
	std::string fixedFieldName(const Field& x);
	static std::string prefixedFieldName(const std::string& prefix, const Field& x);
	static std::string fieldType(const Field& x);
	std::string classNameToId(const Class& x);
	void generateFileList();

private:
	void generateHeader(const Class& x);

	void generateMetaHolder();

	void generateMetaHolderHpp();
	void generateMetaHolderCpp();

	void generateMeta(const Class& x);
	void generateMetaHpp(const Class& x);
	void generateMetaCpp(const Class& x);

	void generateData(const Class& x);
	void generateDataHpp(const Class& x);
	void generateDataCpp(const Class& x);

	void generateOperation(const Class& x);
	void generateOperationHpp(const Class& x);
	void generateOperationCpp(const Class& x);

	void writeIncludes(CppFile& file, const Class& x, const std::string& prefix, const std::string& suffix);

	void writeHppIncludes(CppFile& file, const Class& x, const std::string& folder);
	void writeDataIncludes(CppFile& file, const Class& x);
	void writeMetaIncludes(CppFile& file, const Class& x);
	void writeOperationIncludes(CppFile& file, const Class& x);

	void writeTypeIncludes(CppFile& file, const Class& x);

	std::string classType(const Class& x);
	void read();

	std::string input;
	std::string output;
	Fileset files;
	Project project;
};

} // namespace XProject
} // namespace Nov

#endif // NOV_XPROJECT_GENERATOR_GENERATOR_HPP_
