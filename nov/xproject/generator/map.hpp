#ifndef NOV_XPROJECT_GENERATOR_MAP_HPP_
#define NOV_XPROJECT_GENERATOR_MAP_HPP_

#include <map>
#include <vector>
#include <string>

namespace Nov {
namespace XProject {

template<typename T>
class Map;

template<typename T>
class Link {
public:
	Link() {
	}

	explicit Link(T* x): key(x->id), ptr(x), resolved(true) {
	}

	T& operator*() {
		return mustGet();
	}

	T* operator->() {
		return &mustGet();
	}

	const T& operator*() const {
		return mustGet();
	}

	const T* operator->() const {
		return &mustGet();
	}

	bool isNull() const {
		return !mp && !ptr;
	}

	bool isValid() {
		return !isNull() && get();
	}

	const std::string& id() const {
		return key;
	}

	void init(Map<T>& m) {
		mp = &m;
	}

private:
	friend class Map<T>;

	explicit Link(Map<T>& m, const std::string& id): mp(&m), key(id) {
		// Try to resolve already
		if (!get()) {
			resolved = false;
		}
	}

	T& mustGet() {
		auto p = get();
		if (!p) {
			throw std::logic_error("Could not resolve object " + key);
		}
		return *p;
	}

	T& mustGet() const {
		if (!ptr) {
			throw std::logic_error("Not resolved object " + key);
		}
		return *ptr;
	}
	T* get() {
		if (!resolved) {
			if (!mp) {
				throw std::logic_error("This is null link");
			}
			resolved = true;
			ptr = mp->get(key);
		}
		return ptr;
	}

	Map<T>* mp = nullptr;
	std::string key;
	T* ptr = nullptr;
	bool resolved = false;
};

template<typename T>
void setId(T& x, const std::string& id) {
	x.id = id;
}

template<typename T>
void setId(Link<T>&, const std::string& ) {
}

template<typename T>
class Map {
public:
	Map() {
		ordered.reserve(1024);
	}

	template<typename... Args>
	T* create(const std::string& id, Args&&... args) {
		auto r = data.insert(std::make_pair(id, ordered.size())).second;
		if (r) {
			ordered.emplace_back(std::forward<Args>(args)...);
			setId(ordered.back(), id);
			return &ordered.back();
		}
		return nullptr;
	}

	T* get(const std::string& x) {
		auto i = data.find(x);
		if (i != data.end()) {
			return &ordered[i->second];
		}
		return nullptr;
	}

	Link<T> link(const std::string& x) {
		return Link<T>(*this, x);
	}

	const std::vector<T>& getOrdered() const {
		return ordered;
	}

	template<typename F>
	F forEach(F&& f) {
		for (auto& x: ordered) {
			f(x);
		}
		return f;
	}

	template<typename F>
	F forEach(F&& f) const {
		for (const auto& x: ordered) {
			f(x);
		}
		return f;
	}

	void remap() {
		Index index;
		for (size_t i = 0; i < ordered.size(); ++i) {
			index[ordered[i].id] = i;
		}
		data = std::move(index);
	}

	size_t size() const {
		return ordered.size();
	}

private:
	typedef std::map<std::string, size_t> Index;
	Index data;
	std::vector<T> ordered;
};

} // namespace XProject
} // namespace Nov

#endif // NOV_XPROJECT_GENERATOR_MAP_HPP_
