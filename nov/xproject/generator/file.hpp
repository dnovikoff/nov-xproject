#ifndef NOV_XPROJECT_GENERATOR_FILE_HPP_
#define NOV_XPROJECT_GENERATOR_FILE_HPP_

#include <map>
#include <sstream>
#include <functional>

namespace Nov {
namespace XProject {

class File {
public:
	template<typename T>
	File& operator<<(const T& x) {
		out << x;
		return *this;
	}

private:
	friend class Fileset;

	explicit File(std::ostringstream& o): out(o) {
	}

	std::ostringstream& out;
};

class Fileset {
public:
	File createFile(const std::string& path);

	void saveFiles(const std::string& path);
	void forEach(const std::function<void(const std::string&, const std::string&)>& f);
	bool exists(const std::string& filename);
	std::string readFile(const std::string& filename);
	bool readFile(const std::string& filename, std::string& out);
private:
	std::map<std::string, std::ostringstream> files;
};

class CppFile {
public:
	explicit CppFile(File&& f);
	virtual ~CppFile();

	template<typename T>
	CppFile& operator<<(const T& x) {
		file << x;
		return *this;
	}

protected:
	File file;
};

class HppFile: public CppFile {
public:
	explicit HppFile(File&& f, const std::string& keep);
	virtual ~HppFile();

private:
	std::string keeper;
};

} // namespace XProject
} // namespace Nov

#endif // NOV_XPROJECT_GENERATOR_FILE_HPP_
