#ifndef NOV_XPROJECT_GENERATOR_NAMESPACE_HPP_
#define NOV_XPROJECT_GENERATOR_NAMESPACE_HPP_

#include <boost/range/adaptor/reversed.hpp>
#include <nov/xproject/generator/file.hpp>

namespace Nov {
namespace XProject {

class Namespace {
public:
	explicit Namespace(CppFile& f, const std::vector<std::string>& n): file(f), ns(n) {
		for (const auto& x: ns) {
			file << "namespace " << x << " {\n";
		}
		file << "\n";
	}

	~Namespace() {
		file << "\n";
		for (const auto& x: boost::adaptors::reverse(ns)) {
			file << "} // namespace " << x << "\n";
		}
	}

private:
	CppFile& file;
	const std::vector<std::string>& ns;
};

} // namespace XProject
} // namespace Nov

#endif // NOV_XPROJECT_GENERATOR_NAMESPACE_HPP_
