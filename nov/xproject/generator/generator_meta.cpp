#include "generator.hpp"
#include "namespace.hpp"

namespace Nov {
namespace XProject {

void Generator::generateMeta(const Class& x) {
	generateMetaHpp(x);
	generateMetaCpp(x);
}

void Generator::generateMetaCpp(const Class& x) {
	const auto name = classFileName(x);
	const auto className = metaName(x);
	CppFile out(files.createFile("meta/" + name + ".cpp"));
	out << "#include \"" << name << ".hpp\"" "\n\n";
	out << "#include <nov/database/meta.hpp>" "\n\n";
	writeTypeIncludes(out, x);
	writeMetaIncludes(out, x);
	out << "\n";

	Namespace ns(out, project.ns);
	out << "typedef Nov::Database::Meta Meta;" "\n";

	out << className << "::" << className << "(): Meta(\"" << dataName(x) << "\") {" "\n";
	out << "}" "\n\n";

	out << className << "::~" << className << "() {" "\n";
	out << "}" "\n\n";

	out << "void " << className << "::setInstance(const " << className << "* instance) {" "\n";
	out << "\t" "instance_ = instance;" "\n";
	out << "}" "\n\n";

	out << "const " << className << "* " << className << "::instance() {" "\n";
	out << "\t" "return instance_;" "\n";
	out << "}" "\n\n";

	out << "const " << className << "* " << className << "::instance_ = nullptr;" "\n\n";

	out << "void " << className << "::init() {" "\n";
	x.forEachField([this, &out] (const Field& f) {
		out << "\t" "addField<" << fieldType(f) << " >(" << f.id << ");" "\n";
	});
	out << "\n";

	out << "\t" "Nov::Database::PrimaryKey pk;" "\n";
	for (const auto& p: x.primaryKey.fields) {
		out << "\t" "pk.addField(" << p.second->id << ");" "\n";
	}
	out << "\t" "addKey(pk);" "\n";

	for (const auto& k: x.keys.getOrdered()) {
		out << "\t" "{" "\n";
		out << "\t\t" "Nov::Database::" << ((k.key == Key::Unique)? "Unique" : "Index" )<< "Key key;" "\n";
		for (const auto& p: k.fields) {
			out << "\t\t" "key.addField(" << p.second->id << ");" "\n";
		}
		out << "\t\t" "addKey(key);" "\n";
		out << "\t" "}" "\n";
	}

	if (x.primaryKey.key == Key::Auto) {
		out << "\t" "setSerial(" << x.primaryKey.fields.begin()->second->id << ");" "\n";
	}

	out << "}" "\n\n";

	out << "void " << className << "::initKeys() {" "\n";
	x.foreignKeys.forEach([&out] (const KeyInfo& info) {
		out << "\t" "{" "\n";
		out << "\t\t" "Nov::Database::IndexKey key;" "\n";
		// Suport only single fks
		auto& f = *info.fields.begin()->second;
		out << "\t\t" "key.addField(" << f.id << ");" "\n";
		out << "\t\t" "addForeignKey(key, *" << f.link.id() << "Meta::instance());" "\n";
		out << "\t" "}" "\n";
	});
	out << "}" "\n";
}

void Generator::generateMetaHpp(const Class& x) {
	const auto name = classFileName(x);
	const auto p = "meta/" + name + ".hpp";
	const auto className = metaName(x);
	HppFile out(files.createFile(p), createKeeper(p));
	out << "#include <nov/database/meta.hpp>" "\n";
	out << "#include <nov/database/meta_holder.hpp>" "\n\n";

	Namespace ns(out, project.ns);
	out << "class " << className << ": public Nov::Database::Meta {" "\n";
	out << "public:" "\n";
		out << "\t" "static void setInstance(const " << className << "* instance);" "\n";
		out << "\t" "static const " << className << "* instance();" "\n";
	out << "\n";
		x.forEachField([&out](const Field& f) {
			out << "\t" "NOV_DB_FIELD(" << f.id << ");" "\n";
		});
	out << "private:" "\n";
		out << "\t" "friend class Nov::Database::MetaHolder;" "\n";
		out << "\t" << className << "();" "\n";
		out << "\t~" << className << "();" "\n";
		out << "\t" << className << "(" << className << "&&) = default;" "\n";
		out << "\t" << className << "& operator=(" << className << "&&) = default;" "\n";
		out << "\t" "void init() override;" "\n";
		out << "\t" "void initKeys() override;" "\n";
		out << "\n";
		out << "\t" "static const " << className << "* instance_;" "\n";
	out << "};" "\n";
}

void Generator::generateMetaHolderHpp() {
	const auto p = "meta/holder.hpp";
	HppFile out(files.createFile(p), createKeeper(p));
	out << "#include <nov/database/meta_holder.hpp>" "\n\n";

	Namespace ns(out, project.ns);

	out << "Nov::Database::MetaHolder createMetaHolder();" "\n";
}

void Generator::generateMetaHolderCpp() {
	CppFile out(files.createFile("meta/holder.cpp"));
	out << "#include \"holder.hpp\"" "\n" "\n";

	for (const auto& x: project.classes.getOrdered()) {
		const auto name = classFileName(x);
		out << "#include \"" << name << ".hpp\"" "\n";
	}
	out << "\n";

	Namespace ns(out, project.ns);
	out << "Nov::Database::MetaHolder createMetaHolder() {" "\n";
	out << "\t" "Nov::Database::MetaHolder result;" "\n\n";
	for (const auto& x: project.classes.getOrdered()) {
		const auto meta = metaName(x);
		out << "\t" "result.add<" << meta << ">();" "\n";
	}
	out << "\t" "return result;" "\n";
	out << "}" "\n";
}

void Generator::generateMetaHolder() {
	generateMetaHolderHpp();
	generateMetaHolderCpp();
}

} // namespace XProject
} // namespace Nov
