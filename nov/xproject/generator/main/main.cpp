#include <iostream>
#include <nov/xproject/generator/generator.hpp>

int main(int argc, char **argv) {
	try {
		Nov::XProject::Generator app;
		if (!app.init(argc, argv)) {
			return 1;
		}
		app.run();
	} catch(const std::exception& e) {
		std::cerr << "Stopped with exception: " << e.what() << std::endl;
		return 1;
	}
	return 0;
}
