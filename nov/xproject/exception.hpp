#ifndef NOV_XPROJECT_EXCEPTION_HPP_
#define NOV_XPROJECT_EXCEPTION_HPP_

#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

namespace Nov {
namespace XProject {

class Exception: public std::runtime_error {
public:
	explicit Exception(const std::string& message);
};

void throwException(const std::string& message);


struct Value {
public:
	template<typename T>
	Value(const T& from) {
		std::ostringstream oss;
		oss << from;
		value = oss.str();
	}
	std::string value;
};

struct FieldInfo {
public:
	std::string id;
	std::string value;

	FieldInfo(const std::string& i, const Value& v): id(i), value(v.value) {
	}
};

typedef std::vector<FieldInfo> FieldInfos;

void throwObjectNotFound(const std::string& className, const FieldInfos& infos);

} // namespace XProject
} // namespace Nov

#endif // NOV_XPROJECT_EXCEPTION_HPP_
