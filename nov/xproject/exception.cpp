#include "exception.hpp"
#include "logger.hpp"

namespace Nov {
namespace XProject {

Exception::Exception(const std::string& message): std::runtime_error(message) {
}

void throwException(const std::string& message) {
	logger()->error() << message;
	throw Exception(message);
}

void throwObjectNotFound(const std::string& className, const FieldInfos& infos) {
	std::ostringstream ss;
	ss << "Object '" << className << "' with";

	for (const auto& p: infos) {
		ss << " '" << p.id << "' = " << p.value;
	}

	ss << " not found";
	throwException(ss.str());
}

} // namespace XProject
} // namespace Nov
