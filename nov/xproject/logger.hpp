#ifndef NOV_XPROJECT_LOGGER_HPP_
#define NOV_XPROJECT_LOGGER_HPP_

#include <nov/log/logger.hpp>

namespace Nov {
namespace XProject {

extern Nov::Log::Logger* logger();
extern void setLogger(Nov::Log::Logger* logger);

} // namespace XProject
} // namespace Nov

#endif // NOV_XPROJECT_LOGGER_HPP_
